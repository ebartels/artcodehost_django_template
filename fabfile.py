from __future__ import print_function

import os

from fabric.api import task, run, sudo, local, env, cd, lcd, require, put
from fabric.utils import abort
from fabric.colors import red, green, yellow
from fabric.context_managers import settings, hide, quiet

# Setup django project for use in fabfile
from fabric.contrib import django
django.project('{{project_name}}')
django.settings_module('{{project_name}}.settings')

import {{project_name}}  # noqa

# Env vars
env.hosts = ['{{project_name}}.artcodehost.io']
env.user = 'artcode'
env.password = os.environ.get('PASS')
env.shell = "/bin/bash -c"
env.forward_agent = True
env.site_url = 'https://{{project_name}}.artcodehost.io/'
env.subdomain = '{{project_name}}'
env.database_name = '{{project_name}}'

# Remote paths
env.remote_site_path = '/srv/www/{{project_name}}/'
env.remote_venv_path = os.path.join(env.remote_site_path, 'venv')
env.remote_asset_path = os.path.join(env.remote_site_path, 'htdocs', 'static')

# Local Paths
env.local_project_path = os.path.dirname({{project_name}}.__file__)
env.local_asset_path = os.path.join(env.local_project_path, '..', 'static')


###########
# Helpers #
###########

def argtobool(val):
    """
    Allows for interpretting string arguments as booleans values.
    Since fabric always passes task arguments as strings
    """
    if isinstance(val, bool):
        return val
    if isinstance(val, str):
        return val.lower() in ('true', 'yes', 't', 'y', '1')

    return bool(val)


def tail(logfile, follow=False, n=10, sdo=False):
    if not follow and not n:
        n = 10
    if sdo:
        run_ = sudo
    else:
        run_ = run
    if follow:
        with settings(output_prefix=False):
            run_('tail -n {0} -f {1}'.format(n, logfile))
    else:
        with settings(output_prefix=False):
            run_('tail -n {0} {1}'.format(n, logfile))


@task(alias='venv')
def virtualenv(command, cwd=None):
    """
    Run the command inside a virtualenv.
    """
    require('remote_venv_path')
    activate = os.path.join(env.remote_venv_path, 'bin', 'activate')
    with cd(cwd or env.remote_site_path):
        with settings(output_prefix=False):
            run('source {0} && {1}'.format(activate, command))


@task
def pip(cmd):
    """Run a pip command within the server's virtualenv."""
    virtualenv('pip {0}'.format(cmd))


################
# Django tasks #
################
@task
def djcommand(command):
    """
    Shortcut for running django commands
    """
    require('remote_site_path')
    app_path = os.path.join(env.remote_site_path, 'django')
    virtualenv('python manage.py {0}'.format(command),
               cwd=app_path)


@task
def test(module='{{project_name}}',
         **kwargs):
    """
    Run django tests (continuously on .py file changes)

    Options:
        module: The dotted python path for tests to run
        settings_module: The dotted python path to settings module
        watch: Run tests and watch files (instead of running once)

    """
    settings_module = kwargs.pop('settings_module',
                                 '{{project_name}}.config.tests')
    watch = argtobool(kwargs.pop('watch', False))
    opts = kwargs.pop('opts', '')

    test_command = 'django-admin test --settings={0} {1} {2}' .format(
        settings_module,
        module,
        opts
    )

    with hide('running', 'warnings'):
        if not watch:
            with settings(warn_only=True):
                local(test_command)
        else:
            with settings(warn_only=True):
                local(test_command)

            inot_path = env.local_project_path
            inot_command = (
                'while true; do '
                'inotifywait -qr --exclude \'.*[^\.py]$\' '
                '-e modify,attrib,close_write,move,create,delete '
                '{0} && {1}; done;'.format(inot_path, test_command))

            local(inot_command)


###############
# Local Tasks #
###############
def _generate_secret_key():
    from django.utils.crypto import get_random_string
    chars = 'abcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*(-_=+)'
    secret_key = get_random_string(50, chars)
    return secret_key


@task
def generate_secret_key():
    print(_generate_secret_key())


@task
def generate_local_config():
    """
    Generates config/local.py settings file.
    """
    require('local_project_path')
    config_path = os.path.join(env.local_project_path, 'config', 'local.py')

    if os.path.exists(config_path):
        print(yellow('{0} already exists.'.format(config_path)))
        return

    print('Generating config/local.py')
    secret_key = _generate_secret_key()
    with open(config_path, 'w') as file_:
        file_.write("SECRET_KEY = '{0}'".format(secret_key))


@task
def localdj(*args):
    """Run Django manage.py commands on the local machine."""
    require('local_project_path')
    manage_py = os.path.join(env.local_project_path, '..', 'manage.py')
    cmd = 'python {0} {1}'.format(manage_py, ' '.join(args))
    local(cmd)


@task
def add_django_superuser(username, pw):
    import django
    from django.apps import apps

    django.setup()
    auth_app = apps.get_app_config('auth')
    User = auth_app.get_model('User')

    try:
        User.objects.get(username=username)
        print(yellow("A user '{0}' already exists".format(username)))
    except User.DoesNotExist:
        user = User(
            username=username,
            is_staff=True,
            is_superuser=True,
            is_active=True,
            email='')
        user.set_password(pw)
        user.save()


@task
def load_initial_data():
    """Loads test data into the database for development"""
    add_django_superuser('admin', 'admin')

    require('local_project_path')
    path = os.path.join(env.local_project_path, 'local', 'initial_data.json')
    if os.path.exists(path):
        localdj('loaddata', '-i', path)


@task
def bootstrap():
    """
    Initializes the django project for development
    """
    print(green('Generating config:'))
    generate_local_config()

    print(green('\nSyncing database & running migrations:'))
    localdj('migrate')

    print(green('\nLoading initial data:'))
    load_initial_data()

    with lcd(env.local_project_path):
        node_modules_path = os.path.join(env.local_project_path, '..', 'node_modules')
        if not os.path.exists(node_modules_path):
            print(green('\nInstalling npm modules (npm install):'))
            local('npm install')

    print(green('\nAll Done!\n'))
    print(green('Next steps:\n'))
    print(green('run: ./manage.py runserver'))
    print(green('in another terminal, run: npm start'))
    print(green('visit: http://localhost:8000/'))


################
# Server tasks #
################
@task
def uname():
    """Run uname on the server"""
    run('uname -a')


@task
def checksite():
    """Check that the site is up"""
    require('site_url')
    response = local('curl -L --silent -k -I "%s"' % env.site_url, capture=True)
    if '200 OK' in response:
        print(green('\nSITE IS UP!\n'))
        return True
    else:
        print(red('\nSITE IS DOWN!!'))
        return False


@task
def restart_server():
    """Restart the app server"""
    sudo('systemctl restart {0}_gunicorn'.format(env.subdomain))


@task
def reload_server():
    """Reload the app server"""
    pidfile = os.path.join(
        env.remote_site_path,
        'run/{0}_gunicorn.pid'.format(env.subdomain))
    run('kill -HUP $(cat {0})'.format(pidfile))


@task(alias="tailgun")
def tailgunicorn(follow=False, n=0):
    """Tail the Gunicorn log file."""
    require('remote_site_path')
    tail(os.path.join(env.remote_site_path, 'log/gunicorn.log'), follow=follow, n=n)


@task(alias="tailgerror")
def tailgunicornerror(follow=False, n=0):
    """Tail the Gunicorn log file."""
    require('remote_site_path')
    tail(os.path.join(env.remote_site_path, 'log/gunicorn.error.log'), follow=follow, n=n)


@task(alias='taillog')
def tailaccesslog(follow=False, n=0):
    """Tail the nginx access log file."""
    tail('/var/log/nginx/{{project_name}}.access.log', follow=follow, n=n, sdo=True)


@task(alias='tailerr')
def tailerrorlog(follow=False, n=0):
    """Tail the nginx error log file."""
    tail('/var/log/nginx/error.log', follow=follow, n=n, sdo=True)


@task
def build_assets():
    """
    Build client side assets with npm
    """
    require('local_asset_path')

    # build app assets
    print(green('\nRunning webpack build for app'))
    local('npm run build')


@task
def deploy_assets():
    """
    Deploys static assets to production server
    """
    require('local_asset_path', 'remote_asset_path')

    # run collectstatic on the server
    print(green('\nRunning collectstatic'))
    djcommand('collectstatic --noinput')

    # push client side assets to the server
    print(green('\nDeploying app assets'))
    local_asset_path = os.path.join(env.local_asset_path, 'bundle')
    local_asset_glob = '{0}/*'.format(local_asset_path)
    remote_asset_path = os.path.join(env.remote_asset_path, 'bundle')
    remote_asset_glob = '{0}/*'.format(remote_asset_path)

    assert remote_asset_glob
    assert remote_asset_glob != '/'

    run('rm -rf {0}'.format(remote_asset_glob))
    run('mkdir -p {0}'.format(remote_asset_path))
    put(local_asset_glob, remote_asset_path)


@task
def show_deployed():
    app_path = os.path.join(env.remote_site_path, 'django')
    with cd(app_path), settings(output_prefix=False), hide('running'):
        run('git log -n1 --oneline --decorate')


@task
def deploy(media=True,
           migrate=False,
           requirements=True,
           branch=None,
           full_restart=False):
    """
    Deploy the app to production server.

    Options:
        media:              deploys compiled static assets
        migrate:            run django `migrate` command
        requirements:       pip install -r requirements.txt
        branch:             git branch to pull deploy from (defaults to master)
        full_restart:       do a full restart (instead of graceful reload)
    """
    require('remote_site_path', 'database_name')
    deploy_path = os.path.join(env.remote_site_path, 'django')

    deploy_media = argtobool(media)
    migrate = argtobool(migrate)
    requirements = argtobool(requirements)
    full_restart = argtobool(full_restart)

    # Find current local branch if no branch specified
    if not branch:
        with quiet():
            branch = local('git symbolic-ref --short -q HEAD', capture=True)
    if not branch:
        abort(red("No valid branch is checked out, specify branch= option"))

    print(green('\nDeploying branch "{0}"\n'.format(branch)))

    # Checkout git branch
    print(green('\nUpdating from git repo...\n'))
    with cd(deploy_path):
        run('git fetch origin')
        run('git checkout {0}'.format(branch))
        run('git merge origin/{0}'.format(branch))

    # Update python dependencies
    if requirements:
        print(green('\nUpdating requirements.txt...\n'))
        requirements_file = os.path.join(deploy_path, 'requirements.txt')
        pip('install -r {0}'.format(requirements_file))

    # Run database migrations
    if migrate:
        print(green('\nRunning migrations'))
        djcommand("migrate")

    # Build & deploy media
    if deploy_media:
        # Build and deploy static assets
        print(green('\nBuilding static assets'))
        build_assets()

        print(green('\nDeploying static assets'))
        deploy_assets()

    # Reload server
    print(green('\nReloading server...\n'))
    if full_restart:
        restart_server()
    else:
        reload_server()

    print(green('\nDeployed:'))
    show_deployed()

    print(green('\nALL DONE!'))
