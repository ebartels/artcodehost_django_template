New project template
--------------------

This is a django project template for use with `django startproject`, set up for hosting on artcodehost.io

**Usage:**

1) Install Django (preferably in a virtualenv)

2) Start a new project from this template:

    django-admin startproject $PROJECT_NAME \
        --template=https://bitbucket.org/ebartels/artcodehost_django_template/get/master.zip \
        -e .py -e .json -e .md


Get the code
------------

Clone git repo:

    git clone git@bitbucket.org:ebartels/{{project_name}}.git

Install & build requirements
------------

This project is built on [Django][django] for server-side admin & REST api. The client-side is built using [React][react] and uses [Webpack][webpack] for compiling/bundling modules. Stylesheets are written in [SASS][sass].

* Python 2.7.x is required
* nodejs 6.x is required

See `requirements.txt` and `package.json` for further python & nodejs dependencies.

Top-level directories:
----------------------

`{{project_name}}/`:  Python app - [django][django]  
`app/`:  Client-side app - [react][react]  
`config/`: config files for pip & [webpack][webpack] config files  
`flow/`: configuration for [flow][flow] types  
`static/`: static assets & files  

[django]: http://djangoproject.com/
[react]: https://facebook.github.io/react/  
[webpack]: https://webpack.github.io/
[sass]: http://sass-lang.com/guide  
[flow]: https://flowtype.org/  

Set up dev environment
-------------------------
This project uses [pip][pip] for managing python dependencies. It is strongly recommended you use a [virtualenv][virtualenv] to install into a separate virtual environment.

Example setup:

    cd <path-to-project-root>
    virtualenv .
    source bin/activate

    git clone git@bitbucket.org:ebartels/{{project_name}}.git
    cd {{project_name}}
    pip install -r config/requirements.dev.txt

[pip]: http://pip.readthedocs.org/en/latest/installing.html
[virtualenv]: http://virtualenv.readthedocs.org/en/latest/
    

At this point your should have a working virtualenv with all the project's python dependencies installed.

To finish setting up the project for development, run:  

    fab bootstrap

This Fabric task will: initialize the database, add config files, and run npm install.

Development servers
-------------------

Run the Django dev server in a terminal window:

    ./manage.py runserver

Run webpack dev server in a separate terminal window (recommend using [tmux][tmux] in a terminal split windows):

    npm start

To view the app, go to [http://localhost:8000/](http://localhost:8000/)

[tmux]: https://tmux.github.io/

Managing dependencies
---------------------

To install new python dependencies, use `pip install <package_name>`. Make sure the package is also added to one of the `config/requirements.*.txt` files.

To install new client-side app dependencies use `npm install --save <package_name>`.

Building the app (Webpack)
--------------------------

This project uses Webpack & Babel to build the client-side js & scss files before it can be run inside a browser.  To build the project, run one of the following:

    npm run build       # build production-ready version
    npm run build:dev   # build development version


Deploying
---------

A Fabric task is provided for deploying the project to production. This will require you have ssh access to the host you're deploying the application to.

To deploy, run:

    fab deploy

See fabfile.py for details and usage.
