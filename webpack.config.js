/**
 * Base webpack config
 */
const path = require('path')
const autoprefixer = require('autoprefixer')
const cssnano = require('cssnano')
const BundleTracker = require('webpack-bundle-tracker')
const CleanWebpackPlugin = require('clean-webpack-plugin')
const ExtractCssChunks = require('extract-css-chunks-webpack-plugin')
// const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin

// dev-server  port
const PORT = 3000

const paths = {
  root: path.resolve(__dirname),
  buildPath: path.resolve('./static/bundle'),
  appPath: path.resolve('./app'),
  appStyles: path.resolve('./app/styles'),
}

/**
 * app config
 */
const config = (env = {}) => ({
  name: 'app',
  mode: env.production ? 'production' : 'development',
  context: paths.rootPath,

  entry: {
    main: [
      ...(env.devServer ? [`webpack-dev-server/client?http://localhost:${PORT}`] : []), // hmr
      './app/polyfill',
      './app/index',
    ],
  },

  output: {
    path: paths.buildPath,
    filename: env.production ? '[name].[contenthash:8].js' : '[name].js',
    publicPath: env.devServer ? `http://localhost:${PORT}/bundles/` : '/s/bundle/',
  },

  resolve: {
    extensions: ['.wasm', '.mjs', '.js', '.jsx', '.json'],

    // Allow app imports from '~/some/module'
    alias: {
      '~': paths.appPath,
    },
  },

  module: {
    // Loaders rules for different file types
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            cacheDirectory: true,
            envName: env.production ? 'production' : 'development',
          },
        },
      },
      {
        test: /\.s?css$/,
        use: [
          ExtractCssChunks.loader,
          {
            loader: 'css-loader',
            options: {
              sourceMap: !env.production,
            },
          },
          {
            loader: 'postcss-loader',
            options: {
              sourceMap: !env.production,
              plugins: [
                autoprefixer({ remove: false }),
                ...(env.production ? [cssnano()] : []),
              ],
            },
          },
          {
            loader: 'sass-loader',
            options: {
              sourceMap: !env.production,
              includePaths: [paths.appStyles],
            },
          },
        ],
      },
      {
        test: /\.svg$/,
        exclude: /node_modules/,
        loader: 'svg-react-loader',
      },
      {
        test: /\.(png|gif|jpg|jpeg)?$/,
        use: 'url-loader',
      },
      {
        test: /\.(ttf|eot|woff|woff2)(\?\S*)?$/,
        use: !env.production ? 'url-loader' : {
          loader: 'file-loader',
          options: {
            name: 'fonts/[hash].[ext]',
          },
        },
      },
    ],
  },

  plugins: [
    // Outputs stats on generated files (used by django-webpack-loader)
    new BundleTracker({ filename: './static/bundle/webpack-stats.json' }),

    new ExtractCssChunks({
      filename: `[name]${env.production ? '.[contenthash:8]' : ''}.css`,
      hot: env.devServer,
    }),

    // production only
    ...(env.production ? [
      // new BundleAnalyzerPlugin({ analyzerMode: 'server' }),
    ] : []),

    // dev & production, but not for dev-server
    ...(!env.devServer ? [
      new CleanWebpackPlugin([paths.buildPath], {
        exclude: ['webpack-stats.json', 'editor_content.css'],
        beforeEmit: true,
      }),
    ] : []),
  ],

  optimization: {
    splitChunks: {
      cacheGroups: {
        // vendor.js - node_modules, excluding css modules
        commons: {
          name: 'vendor',
          chunks: 'initial',
          test: module => (
            module.context.indexOf('/node_modules/') !== -1 &&
            !/\.s?css$/.test(module.nameForCondition && module.nameForCondition())
          ),
        },
      },
    },
  },

  // production only settings
  ...(env.production ? {
    performance: {
      maxEntrypointSize: 360000,
      maxAssetSize: 300000,
    },
  } : {}),

  ...(!env.production ? {
    devtool: 'inline-cheap-module-source-map',
  } : {}),

  // devServer settings
  ...(env.devServer ? {
    devServer: {
      port: PORT,
      headers: {
        'Access-Control-Allow-Origin': '*',
      },
      // static assets are served by django
      proxy: {
        '/s': 'http://localhost:8000',
      },
    },
  } : {}),
})

/**
 * ckeditor content css config
 */
const editorConfig = (env = {}) => ({
  ...config(env),
  ...({
    name: 'editor',
    entry: {
      editor_content: './app/styles/editor_content.scss',
    },
    output: {
      path: paths.buildPath,
      filename: '[name].js',
    },
    plugins: [
      new ExtractCssChunks({
        filename: '[name].css',
      }),
    ],
  }),
})

module.exports = (env) => ([
  config(env),
  editorConfig(env),
])
