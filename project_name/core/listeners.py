from __future__ import unicode_literals

from django.apps import apps
from django.db.models import signals
from django.dispatch import receiver
from django.conf import settings

from sorl import thumbnail

from ado.media.models import Image
from {{project_name}}.core import signals as core_signals


IMAGE_SIZES = settings.SCALED_IMAGE_SIZES


@receiver(signals.post_save, sender=Image)
def pre_render_image_thumbs(instance, **kwargs):
    """
    Pre-generate thumbnail in various sizes when an image is saved
    """
    if not kwargs.get('raw') and not getattr(settings, 'TESTING', False):
        for key, size in IMAGE_SIZES:
            thumbnail.get_thumbnail(instance.filename, size)
