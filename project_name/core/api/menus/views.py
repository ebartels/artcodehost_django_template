from rest_framework.views import APIView
from rest_framework.response import Response
from ado.menus.models import MenuItem


class MenuApiView(APIView):

    def get_menu_item(self, item):
        children = [self.get_menu_item(i)
                    for i in item.get_children()]

        url = item.get_absolute_url()
        if not url and len(children):
            url = children[0]['url']

        return {
            'id': item.id,
            'title': item.title,
            'slug': item.slug,
            'url': url,
            'children': children,
            'parent': item.parent and item.parent.slug,
            'level': item.level,
        }

    def get_menu_tree(self):
        items = MenuItem.objects.root_nodes()
        return [self.get_menu_item(item) for item in items]

    def get(self, request, format=None):
        return Response(self.get_menu_tree())


menu = MenuApiView.as_view()
