import datetime

from django.core.cache import cache
from django.utils.encoding import force_text

from rest_framework_extensions.cache.decorators import cache_response
from rest_framework_extensions.key_constructor.constructors import (
    KeyConstructor
)
from rest_framework_extensions.key_constructor import bits


# This module uses drf-extensions package to construct cached responses
# See: https://chibisov.github.io/drf-extensions/docs/#key-constructor
# This code was adapted from example code in drf-extensions docs


class UpdatedAtKeyBit(bits.KeyBitBase):
    """
    Key bit that keeps track of last updated timestamp.  This timestamp can
    easily be changed to invalidate the cache on model changes. For example:

    cache.set('api_updated_at_timestamp', value=datetime.utcnow())

    ... will update the timestamp and invalidate previous cached items. This
    can be done up in model signal listenrs for invalidation.
    """
    CACHE_KEY = 'api_updated_at_timestamp'

    def get_data(self, **kwargs):
        value = cache.get(self.CACHE_KEY, None)
        if not value:
            value = datetime.datetime.utcnow()
            cache.set(self.CACHE_KEY, value=value, timeout=None)
        return force_text(value)


def change_api_updated_at():
    cache.set(UpdatedAtKeyBit.CACHE_KEY,
              datetime.datetime.utcnow(),
              timeout=None)


class CustomKeyConstructor(KeyConstructor):
    """
    Gengeric key constructor suitable for any drf view
    """
    unique_method_id = bits.UniqueMethodIdKeyBit()
    format = bits.FormatKeyBit()
    args = bits.ArgsKeyBit('*')
    kwargs = bits.KwargsKeyBit('*')
    query_params = bits.QueryParamsKeyBit('*')
    updated_at = UpdatedAtKeyBit()


class CustomListKeyConstructor(CustomKeyConstructor):
    """
    A key constructor suitable for drf ListApiView. This constructor adds the
    SQL for the queryset used to render the view. This is useful in case
    queryset is changed in code, which will trigger a cache invalidation.
    """
    list_sql = bits.ListSqlQueryKeyBit()


class CustomRetrieveKeyConstructor(CustomKeyConstructor):
    """
    A key constructor suitable for drf RetrieveAPIView. This constructor adds
    the SQL for the queryset used to render the view. This is useful in case
    queryset is changed in code, which will trigger a cache invalidation.
    """
    retrieve_sql = bits.RetrieveSqlQueryKeyBit()


class CacheResponseMixin(object):
    """
    A generic drf view mixin which caches GET requests.
    """

    @cache_response(key_func=CustomKeyConstructor())
    def get(self, request, *args, **kwargs):
        return super(CacheResponseMixin, self).get(request, *args, **kwargs)


class ListCacheResponseMixin(object):
    """
    A drf view mixin specialized for ListApiView which caches GET requests.
    """

    @cache_response(key_func=CustomListKeyConstructor())
    def get(self, request, *args, **kwargs):
        return super(ListCacheResponseMixin, self).get(request, *args, **kwargs)


class RetrieveCacheResponseMixin(object):
    """
    A drf view mixin specialized for RetrieveAPIView which caches GET requests.
    """

    @cache_response(key_func=CustomRetrieveKeyConstructor())
    def get(self, request, *args, **kwargs):
        return super(RetrieveCacheResponseMixin, self).get(request, *args, **kwargs)
