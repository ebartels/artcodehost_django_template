"""
Settings for use in production.
"""
import os
from .defaults import *

DEBUG = False

MANAGERS = list(MANAGERS) + [
    # Add any extra email addresses
]

ALLOWED_HOSTS = (
    # Add any domains that will be served in production
    # '.foo.com',
    '{{project_name}}.artcodehost.io',
)

# Cache Setup
CACHE_TIMEOUT = 60 * 10
CACHE_PREFIX = '{{project_name|upper}}'
CACHES = {
    'default': {
        "BACKEND": "django_redis.cache.RedisCache",
        'LOCATION': '127.0.0.1:6379',
        'KEY_PREFIX': CACHE_PREFIX,
        'TIMEOUT': CACHE_TIMEOUT,
        'OPTIONS': {
            'PARSER_CLASS': 'redis.connection.HiredisParser',
            'CONNECTION_POOL_CLASS': 'redis.BlockingConnectionPool',
            'CONNECTION_POOL_KWARGS': {
                'max_connections': 50,
                'timeout': 20,
            },
            'MAX_ENTRIES': 10000,
        },
    }
}
CACHE_MIDDLEWARE_SECONDS = CACHE_TIMEOUT
CACHE_MIDDLEWARE_KEY_PREFIX = CACHE_PREFIX
SESSION_ENGINE = "django.contrib.sessions.backends.cached_db"

# Cached template loader
# TEMPLATES[0]['OPTIONS']['loaders'] = [
#     ('django.template.loaders.cached.Loader', [
#         'django.template.loaders.filesystem.Loader',
#         'django.template.loaders.app_directories.Loader',
#     ]),
# ]

# Temp folders
TEMP_DIR = '/tmp/'
FILE_UPLOAD_TEMP_DIR = TEMP_DIR

# AWS Storage Settings
AWS_STORAGE_BUCKET_NAME = '{{project_name}}-media-w2'
AWS_S3_REGION_NAME = 'us-west-2'
AWS_S3_HOST = 's3-us-west-2.amazonaws.com'
AWS_QUERYSTRING_AUTH = False
AWS_PRELOAD_METADATA = False
AWS_HEADERS = {
    'Cache-Control': 'max-age=864000',
}
# AWS_S3_CUSTOM_DOMAIN = 'YOUR_SUBDOMAIN.cloudfront.net'  # cloudfront

# File Storage
DEFAULT_FILE_STORAGE = 'storages.backends.s3boto.S3BotoStorage'
MEDIA_ROOT = os.path.join(TEMP_DIR, 'media')
MEDIA_URL = 'https://{{project_name}}-media-w2.s3-us-west-2.amazonaws.com/'

STATIC_ROOT = root('../../htdocs/static')
STATIC_URL = '/s/'

# This fixes performance problems with sorl-thumbnail when used with s3
THUMBNAIL_FORCE_OVERWRITE = True

WEBPACK_LOADER = {
    'DEFAULT': {
        'BUNDLE_DIR_NAME': 'bundle/',
        'STATS_FILE': os.path.join(STATIC_ROOT, 'bundle/webpack-stats.json'),
    },
    'CUSTOMADMIN': {
        'BUNDLE_DIR_NAME': 'customadmin/bundle/',
        'STATS_FILE': os.path.join(STATIC_ROOT, 'customadmin/bundle/webpack-stats.json'),
    },
}
