"""
local settings.
Add/override any settings here.
NOTE: This file should not be checked into source control
"""

# Make this unique, and don't share it with anybody.
SECRET_KEY = '{{secret_key}}'

# Database Settings
# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.postgresql_psycopg2',
#         'NAME': '{{project_name}}',
#         'USER': 'mrmelve',
#         'PASSWORD': '',
#         'HOST': '',
#         'PORT': '',
#     },
# }
