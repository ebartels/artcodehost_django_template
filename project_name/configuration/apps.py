from __future__ import unicode_literals

from django.apps import AppConfig


class ConfigurationConfig(AppConfig):
    name = '{{project_name}}.configuration'
    verbose_name = 'Settings'
    icon = '<i class="icon material-icons">settings</i>'
