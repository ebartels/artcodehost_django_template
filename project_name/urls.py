from django.conf.urls import include, url
from django.conf.urls.static import static
from django.views.generic import TemplateView
from django.conf import settings
from django.contrib import admin

# from ado import menus

urlpatterns = [
    url(r'^$', TemplateView.as_view(template_name='index.html'), name='index'),

    # url(r'^pages/', include('{{project_name}}.pages.urls')),
    # ado
    url(r'^media/', include('ado.media.urls')),

    # Admin:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^customadmin/', include('ado.customadmin.urls')),
    url(r'^admin/media/', include('ado.media.admin.urls')),
    url(r'^admin/', include(admin.site.urls)),
]

# API Urls
# urlpatterns += [
#     url(r'^api/', include('{{project_name}}.core.api.urls', namespace="api")),
# ]

# MEDIA For Development Server
urlpatterns += static(settings.MEDIA_URL,
                      document_root=settings.MEDIA_ROOT,
                      show_indexes=True)
